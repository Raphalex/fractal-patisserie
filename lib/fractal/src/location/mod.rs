//! Module for dealing with locations in fractals.

mod complex_window;
pub use complex_window::{ComplexWindow, Resolution};

use num_complex::Complex;

use crate::ComplexFloatType;

/// A location in a fractal.
#[derive(Debug, Default, Clone, Copy)]
pub struct Location<T = f64>
where
    T: ComplexFloatType,
{
    /// Position of this location in the complex set.
    pub pos: Complex<T>,
    /// Zoom level of this location.
    ///
    /// The zoom level is considered like this:
    /// - a `zoom` value of 1 corresponds to a square in the complex set with sides of size 2.
    /// - the window shrinks exponentially, so a `zoom` value of 2 would mean
    /// that square have sides of size 1.
    ///
    /// # For developers
    /// Please note that this window isn't really represented in the [Location] struct, but
    #[allow(rustdoc::private_intra_doc_links)]
    /// makes sense with the [ComplexWindow::start] and [ComplexWindow::end] fields.
    pub zoom: T,
}

impl<T> Location<T>
where
    T: ComplexFloatType,
{
    /// Returns a new location.
    /// # Arguments
    /// See the corresponding fields of the [Location] struct.
    pub fn new(pos: Complex<T>, zoom: T) -> Self {
        Self { pos, zoom }
    }
}

pub mod locators {
    //! Locator traits are interfaces to get interesting locations in a fractal set.

    use super::Location;
    use crate::ComplexFloatType;

    /// Locator that returns a default location.
    ///
    /// The default location usually corresponds to a general, zoomed-out view of the fractal.
    pub trait DefaultLocator<T>
    where
        T: ComplexFloatType,
    {
        /// Get the default location from this locator.
        fn get_default_location(&self) -> Location<T>;
    }

    /// A locator that can generate a random sequence of interesting locations when given a seed.
    pub trait RandomLocator<T>
    where
        T: ComplexFloatType,
    {
        /// Type to use as a seed.
        type Seed;

        /// Return the next location in the sequence
        ///
        /// The reason this is a mutable reference to `self` is because
        /// most Random Number Generators need to mutate in order to prepare for the next random   
        /// numbers.
        fn get_next_random_location(&mut self) -> Location<T>;

        /// Sets the seed for this locator.
        /// # Arguments
        /// * `seed` - The seed to set.
        fn set_seed(&mut self, seed: Self::Seed);

        /// Returns a reference to the seed currently used by this locator.
        fn get_seed(&self) -> &Self::Seed;
    }
}
