//! Structures related to views (rectangle windows) into the set of complex
//! numbers (basically, where fractals live).

use num_complex::Complex;

use crate::{image::FractalImage, location::Location, ComplexFloatType, Fractal, FractalGenInfo};

/// Resolution type with a width and a height.
///
/// Defined on unsigned integers.
#[derive(Debug, Default, Clone, Copy)]
pub struct Resolution {
    /// Width for this resolution.
    pub width: u32,
    /// Height for this resolution.
    pub height: u32,
}

/// Structure representing a window (view) on complex numbers.
#[derive(Debug, Default, Clone, Copy)]
pub struct ComplexWindow<T = f64>
where
    T: ComplexFloatType,
{
    /// Location of the private window
    pub location: Location<T>,
    /// Resolution of the window ()
    pub resolution: Resolution,
    /// Coordinate of the upper-left corner of the window.
    start: Complex<T>,
    /// Coordinate of the bottom-right corner of the window.
    end: Complex<T>,
}

impl<T> ComplexWindow<T>
where
    T: ComplexFloatType,
{
    /// Return a new ComplexWindow.
    /// # Arguments
    /// Each argument corresponds to the members of the same name described in the struct.
    pub fn new(location: Location<T>, resolution: Resolution) -> Self {
        let mut window = ComplexWindow {
            location,
            resolution,
            ..Default::default()
        };
        window.compute_start_end();
        window
    }

    /// Convert a pixel position to a complex position.
    /// # Arguments
    /// * `i` - X pixel position (horizontal, 0 on the left).
    /// * `j` - Y pixel position (vertical, 0 on the top).
    pub fn pix_to_comp(&self, i: u32, j: u32) -> Complex<T> {
        // Compute the size of the complex window
        let complex_size = self.end - self.start;
        // Compute the step that separates consecutive pixels in both directions
        let step = (
            complex_size.re / T::from(self.resolution.width).unwrap(),
            complex_size.im / T::from(self.resolution.height).unwrap(),
        );
        Complex::new(
            self.start.re + (T::from(i).unwrap() * step.0),
            self.start.im + (T::from(j).unwrap() * step.1),
        )
    }

    /// Compute the start and end corners of a window from its position, zoom, and resolution.
    fn compute_start_end(&mut self) {
        // Start by translating the window by its center position
        self.start = Complex::<T>::new(-T::one(), -T::one()) + self.location.pos;
        self.end = Complex::<T>::new(T::one(), T::one()) + self.location.pos;

        // Compute aspect ratio
        let ratio =
            T::from(self.resolution.height).unwrap() / T::from(self.resolution.width).unwrap();
        // Compute real size of window
        let size = self.end - self.start;
        // Resize window according to aspect ratio
        let lost = size.re - size.re * ratio;
        // Move start point to "compensate" for "lost" image from aspect ratio conversion
        self.start.im += lost / T::from(2.).unwrap();
        // Apply aspect ratio
        self.end.im = self.start.im + size.re * ratio;

        // Recompute the size since it changed because of applying the aspect ratio
        let size = self.end - self.start;
        // Resize window according to zoom
        self.start +=
            size / T::from(2.).unwrap() - size / (T::from(2.).unwrap() * self.location.zoom);
        self.end -=
            size / T::from(2.).unwrap() - size / (T::from(2.).unwrap() * self.location.zoom);
    }

    /// Generate a fractal image from a fractal and the necessary information.
    /// # Arguments
    /// * `fgi` - The necessary information to generate the image (see [FractalGenInfo]).
    pub fn to_fractal_image<F: Fractal<T>>(self, fgi: &FractalGenInfo) -> FractalImage<T> {
        let Resolution { width, height } = self.resolution;
        let mut img = FractalImage::<T>::new(width as usize, height as usize);
        for i in 0..height {
            for j in 0..width {
                // Simply compute the fractal result for each pixel and push it in the fractal image
                img[(i as usize, j as usize)] = F::get_point_value(self.pix_to_comp(j, i), fgi);
            }
        }
        img
    }
}
