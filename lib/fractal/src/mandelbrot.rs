//! Implementation of the Mandelbrot set.

use num_complex::Complex;
use rand::{random, rngs::StdRng, Rng, SeedableRng};

use crate::{
    location::{
        locators::{DefaultLocator, RandomLocator},
        ComplexWindow,
    },
    location::{Location, Resolution},
    ComplexFloatType, Fractal, FractalGenInfo,
};

/// Implementation of the Mandelbrot set.
///
/// See <https://en.wikipedia.org/wiki/Mandelbrot_set>
pub struct Mandelbrot {}

impl Mandelbrot {
    /// Returns a reasonable limit for the given location.
    ///
    /// Given a location (especially a `zoom`), this fonction computes a limit.
    /// The bigger the zoom, the bigger the limit; this gives a sharp resolution
    /// for the most zoomed-in locations.
    ///
    /// # Arguments
    /// * `location` - The location to compute this limit for.
    pub fn get_ideal_limit<T: ComplexFloatType>(location: &Location<T>) -> u32 {
        const DEFAULT_LIMIT: u32 = 350;
        let default_limit = unsafe { T::from(DEFAULT_LIMIT).unwrap_unchecked() };
        // This is totally a heuristic, but it gives ok results.
        (default_limit * (location.zoom.log10() / T::from(2.).unwrap()))
            .max(default_limit)
            .to_u32()
            .unwrap()
    }
}

impl<T> Fractal<T> for Mandelbrot
where
    T: ComplexFloatType,
{
    const THRESH: f64 = 1000000.;
    fn sequence_step(
        z: num_complex::Complex<T>,
        c: num_complex::Complex<T>,
    ) -> num_complex::Complex<T> {
        // The mandelbrot sequence
        z * z + c
    }
    fn init_sequence() -> num_complex::Complex<T> {
        // The sequence starts at 0
        Complex::<T>::new(T::zero(), T::zero())
    }
}

/// Locator implementation for the Mandelbrot set.
///
/// # Random location algorithm
/// In order to get a random location in this set, the following algorithm is used:
/// * a window is initialized using this locator's [DefaultLocator::get_default_location];
/// * the fractal is rendered in a minimal resolution (for example, 50x50);
/// * random points are picked in the fractal window until we find a point which is on the boundary
///   of the fractal (meaning it has at least one neighbour in the set and one outside the set);
/// * the window is centered on the boundary point and zoomed-in
/// * the above operations are repeated a random number of times (in a reasonable range)
pub struct MandelbrotLocator {
    /// Seed for this random locator.
    seed: u64,
    /// The Random Number Generator to use.
    rng: StdRng,
}

impl MandelbrotLocator {
    /// Returns a new locator with a random seed.
    pub fn new() -> Self {
        let seed = random();
        Self {
            seed,
            rng: StdRng::seed_from_u64(seed),
        }
    }

    /// Returns a now locator with the specified seed.
    ///
    /// # Arguments
    /// * `seed` - The seed to use.
    pub fn with_seed(seed: u64) -> Self {
        Self {
            seed,
            rng: StdRng::seed_from_u64(seed),
        }
    }

    /// Returns whether the specified point is on the boundary of the fractal.
    ///
    /// # Arguments
    /// * `(x, y)` - The pixel coordinates of the point to test
    /// * `window` - The window considered for this calculation
    fn is_on_boundary<T: ComplexFloatType>((x, y): (u32, u32), window: &ComplexWindow<T>) -> bool {
        let (x, y) = (x as i32, y as i32);
        let (min_x, min_y) = (0, 0);
        let (max_x, max_y) = (
            window.resolution.width as i32,
            window.resolution.height as i32,
        );
        let fgi = FractalGenInfo::new(Mandelbrot::get_ideal_limit(&window.location), None);
        let (mut neighbour_in, mut neighbour_out) = (false, false);
        // Loop through neighbours
        for i in -1..=1 {
            for j in -1..=1 {
                let neighbour = window.pix_to_comp(
                    (i + x).clamp(min_x, max_x) as u32,
                    (j + y).clamp(min_y, max_y) as u32,
                );
                // Test whether the neighbour is in the set or not
                if Mandelbrot::get_point_value(neighbour, &fgi).iterations == fgi.limit {
                    neighbour_in = true;
                } else {
                    neighbour_out = true;
                }
            }
        }
        neighbour_in && neighbour_out
    }
}

impl<T> DefaultLocator<T> for MandelbrotLocator
where
    T: ComplexFloatType,
{
    fn get_default_location(&self) -> Location<T> {
        Location::new(Complex::new(T::from(-0.5).unwrap(), T::zero()), T::one())
    }
}

impl<T> RandomLocator<T> for MandelbrotLocator
where
    T: ComplexFloatType,
{
    type Seed = u64;
    fn set_seed(&mut self, seed: Self::Seed) {
        self.seed = seed;
        self.rng = StdRng::seed_from_u64(seed);
    }

    fn get_seed(&self) -> &Self::Seed {
        &self.seed
    }

    fn get_next_random_location(&mut self) -> Location<T> {
        // Start with the default Mandelbrot location
        let mut location = self.get_default_location();
        let resolution = Resolution {
            width: 50,
            height: 50,
        };
        // Pick a random number of iterations
        let iterations = self.rng.gen_range(1..40);
        for _ in 0..iterations {
            // Zoom-in
            location.zoom *= T::from(2.).unwrap();
            let window = ComplexWindow::<T>::new(location, resolution);
            // Find a pixel on the boundary of the set
            let pix = {
                // This can loop forever; in case no pixel is found under a
                // certain number a iterations, just return the current
                // location.
                // Let's say the max number of attempts is the number of pixels in the window.
                let max_attempts = resolution.width * resolution.height;
                let mut attempts = 0;
                loop {
                    let pix = (
                        self.rng.gen_range(0..(resolution.width - 1)),
                        self.rng.gen_range(0..(resolution.height - 1)),
                    );
                    let is_on_boundary = Self::is_on_boundary(pix, &window);
                    if is_on_boundary {
                        break pix;
                    }
                    if attempts == max_attempts {
                        return location;
                    }
                    attempts += 1;
                }
            };
            // Move the position to center on the boundary pixel
            location.pos = window.pix_to_comp(pix.0, pix.1)
        }
        location
    }
}
