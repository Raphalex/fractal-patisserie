//! 2D Vector type.

use std::ops::{Index, IndexMut};

/// A 2D Vector.

/// Abstraction for 2-dimensional vector with known size at initialization time.
///
/// This struct contains a single vector that has special indexing to be used
/// like 2D vector without the overhead of a Vec<Vec>.
///
/// The data can be indexed with tuples :
/// # Examples
/// ```ignore
/// let mut vec2d = Vector2D::<i32>::new(10, 10);
/// vec2d[(2, 2)] = 42;
/// assert_eq!(42, vec2d[(2, 2)]);
/// ```
#[derive(Debug)]
pub struct Vector2D<T>
where
    T: Default,
{
    pub data: Vec<T>,
    pub width: usize,
    pub height: usize,
}

impl<T> Vector2D<T>
where
    T: Default,
{
    pub fn new(width: usize, height: usize) -> Self {
        Self {
            data: {
                let mut data = Vec::new();
                data.resize_with(width * height, Default::default);
                data
            },
            width,
            height,
        }
    }
}

impl<T> Index<(usize, usize)> for Vector2D<T>
where
    T: Default,
{
    type Output = T;
    fn index(&self, index: (usize, usize)) -> &Self::Output {
        &self.data[index.0 * self.width + index.1]
    }
}

impl<T> IndexMut<(usize, usize)> for Vector2D<T>
where
    T: Default,
{
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        &mut self.data[index.0 * self.width + index.1]
    }
}
