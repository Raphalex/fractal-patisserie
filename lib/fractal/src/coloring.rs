//! Everything related to coloring methods on fractals.

use crate::{image::FloatImageType, ComplexFloatType, FractalGenInfo, FractalResult};

/// Interface to deal with generation of actual images from fractal images
///
/// The [Coloring::result_to_color] function maps a [FractalResult] to a position on a color gradient.
pub trait Coloring<T, U>
where
    T: FloatImageType,
    U: ComplexFloatType,
{
    /// Returns a mixing factor (value from `0.` to `1.`) from a [FractalResult].
    /// # Argument
    /// * `fr` - The [FractalResult] to convert.
    /// * `fgi` - The info used to generate the [FractalResult]s.
    fn result_to_color(fr: &FractalResult<U>, fgi: &FractalGenInfo) -> T;
}

pub mod methods {
    //! Different coloring methods for fractals

    use crate::{image::FloatImageType, Coloring, ComplexFloatType, FractalGenInfo, FractalResult};

    /// Standard fractal coloring.
    ///
    /// Color is determined by how many iterations it took for the fractal
    /// sequence to escape, relative to the maximum number of iterations.
    pub struct Standard {}

    impl<T, U> Coloring<T, U> for Standard
    where
        T: FloatImageType,
        U: ComplexFloatType,
    {
        fn result_to_color(fr: &FractalResult<U>, fgi: &FractalGenInfo) -> T {
            // Standard coloring is simply about dividing the number of iterations by the limit
            unsafe {
                T::from(fr.iterations).unwrap_unchecked() / T::from(fgi.limit).unwrap_unchecked()
            }
        }
    }
}
