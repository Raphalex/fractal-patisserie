//! A library for generating fractals under differents forms, and with different coloring methods.
//!
//! # Overview
//!
//! The fractal library allows you to generate images from pre-implemented
//! fractal types (for example, the Mandelbrot set). You can also implement your
//! own fractals by implementing the [Fractal] trait.
//!
//! Typical code to generate a fractal would look like this:
//! ```no_run
//! use fractal::location::locators::RandomLocator;
//! use fractal::{
//!     coloring::methods::Standard,
//!     location::{ComplexWindow, Resolution},
//!     mandelbrot::{Mandelbrot, MandelbrotLocator},
//!     FractalGenInfo,
//! };
//! use fractal::{Gradient, LinSrgb};
//!
//! fn main() {
//! // First, we choose the location to render in the fractal.
//! // For this, we're gonna use a locator. This one is made for the Mandelbrot set:
//! let mut locator = MandelbrotLocator::new();
//!
//! // Next, create a ComplexWindow, which is basically a view into the set of complex numbers.
//! // Here, the window uses a random location that was returned from the locator we created
//! // earlier and has a resolution of 256 x 256 pixels.
//! let window: ComplexWindow = ComplexWindow::new(
//!     locator.get_next_random_location(),
//!     Resolution {
//!         width: 256,
//!         height: 256,
//!     },
//! );
//!
//! // Next, create info to generate the wanted fractal. These usually include
//! // the maximum number of iterations for the fractal sequence, and the number
//! // of values obtained in the sequence to keep in memory. Those values are used
//! // by some (but not all) coloring methods.
//!
//! // The Mandelbrot struct has a helper function to compute a limit for us, so let's use that:
//! let fgi: fractal::FractalGenInfo =
//!     FractalGenInfo::new(Mandelbrot::get_ideal_limit(&window.location), None);
//!
//! // Given a ComplexWindow and a Fractal type, we can generate a FractalImage
//! // using the generation info we defined earlier.
//! let fractimg = window.to_fractal_image::<Mandelbrot>(&fgi);
//! // The fractal image can then be converted to a float image, by specifying
//! // a coloring method.
//! let floatimg = fractimg.to_float_image::<Standard, f64>(&fgi);
//! // The float image can then be converted to an actual RGB image (see the `image` crate for this)
//! let img = floatimg.to_rgb_image(&Gradient::<LinSrgb<f64>>::new(vec![
//!     LinSrgb::new(0., 0., 0.),
//!     LinSrgb::new(1., 1., 1.),
//! ]));
//! // Finally, using the `image` crate, we can save the image to a file in the wanted format.
//! img.save("output.png")
//!     .expect("Failed to save the generated image to a file");
//! }
//!
//! ```
#![deny(missing_docs)]

use num_traits::{Float, NumAssign, NumCast};
use std::fmt::Debug;

// Re-export some crates and types that are useful for users.
pub use num_complex::{Complex, Complex32, Complex64};
pub use palette::{Gradient, LinSrgb};

mod image;
pub use self::image::{FloatImage, FloatImageType, FractalImage};
mod utils;

pub mod coloring;
pub use coloring::Coloring;
pub mod location;
pub mod mandelbrot;

/// Super trait to compile everything needed for types used with the [Complex] type.
pub trait ComplexFloatType: Float + NumAssign + NumCast + Copy + Default + Debug {}
impl<T> ComplexFloatType for T where T: Float + NumAssign + NumCast + Copy + Default + Debug {}

/// Information for fractal generation.
#[derive(Debug, Clone, Copy)]
pub struct FractalGenInfo {
    /// Maximum number of iterations until a complex number is considered not in the fractal set.
    limit: u32,
    /// Path length to generate for each point computation.
    path_length: Option<u32>,
}

impl FractalGenInfo {
    /// Returns a new [FractalGenInfo].
    ///
    /// # Arguments
    /// * `limit` - The limit (max number of iterations) for computation of the fractal sequence.
    /// * `path_length` - The number of values from the fractal sequence to keep in the
    /// [FractalResult]s.
    //TODO: Change the path length to be of different enum types: none, last, full
    pub fn new(limit: u32, path_length: Option<u32>) -> Self {
        Self { limit, path_length }
    }
}

/// Interface of a fractal.
///
/// Basically, we define a fractal here as something which, given a complex
/// point, a threshold and a limit, can return the number of iterations of some
/// operation needed for the initial value to surpass the threshold ( for an
/// example, see <https://en.wikipedia.org/wiki/Mandelbrot_set>).
pub trait Fractal<T = f64>
where
    T: ComplexFloatType,
{
    /// Threshold above which points escape the fractal set.
    const THRESH: f64;
    /// Returns the result of applying the fractal sequence on a given complex point.
    /// # Arguments
    /// * `c` - The input complex point.
    /// * `limit` - Maximum number of iterations to compute. An input point that
    /// reaches this limit is considered in the fractal set.
    /// * `thresh` - Threshold over which iterations stop.
    /// * `path_length` - Number of computed points (from the last interation to
    /// the first) to return along with the number of iterations (see [FractalResult::path]). Defaults to
    /// `0` if `None` is passed.
    fn get_point_value(c: Complex<T>, fgi: &FractalGenInfo) -> FractalResult<T> {
        let path_length = match fgi.path_length {
            Some(l) => l,
            None => 0,
        };
        let mut res: FractalResult<T> = Default::default();
        let mut z = Self::init_sequence();
        let thresh_squared = Self::get_thresh().powi(2);
        while res.iterations < fgi.limit {
            res.add_point(if path_length == 0 { None } else { Some(z) });
            z = Self::sequence_step(z, c);
            if z.norm_sqr() > thresh_squared {
                break;
            }
        }
        res
    }

    /// Returns the threshold of the fractal, converted to type T.
    #[inline]
    fn get_thresh() -> T {
        unsafe { T::from(Self::THRESH).unwrap_unchecked() }
    }

    /// Returns the next value of the fractal sequence.
    /// # Arguments
    /// `z` - The previous value in the sequence.
    /// `c` - The point for which the sequence is computed.
    fn sequence_step(z: Complex<T>, c: Complex<T>) -> Complex<T>;

    /// Returns the initial value of the fractal sequence
    fn init_sequence() -> Complex<T>;
}

/// Result of a fractal calculation on a complex point.
#[derive(Default, Debug)]
pub struct FractalResult<T = f64>
where
    T: ComplexFloatType,
{
    /// List of the complex values computed while iterating through the fractal
    /// sequence (from last to first).
    pub path: Vec<Complex<T>>,
    /// Number of iterations completed before the complex passed the threshold of the fractal (see [Fractal::get_point_value]).
    pub iterations: u32,
}

impl<T> FractalResult<T>
where
    T: ComplexFloatType,
{
    /// Add a point to the path (optionnaly), and increment the iteration count.
    /// # Arguments
    /// `point` - The point to add to the path (or `None`)
    pub fn add_point(&mut self, point: Option<Complex<T>>) {
        if let Some(p) = point {
            self.path.push(p);
        }
        self.iterations += 1;
    }
}
