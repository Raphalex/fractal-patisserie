//! Fractal images module
//!
//! The are multiple types of images, in a typical use case, users would want to go from a [FractalImage], to a [FloatImage], to an actual [RgbImage].

use image::{Rgb, RgbImage};
use palette::FromF64;
use palette::{float::Float, Component, Gradient, LinSrgb};

use crate::utils::Vector2D;
use crate::{Coloring, ComplexFloatType, FractalGenInfo, FractalResult};

/// Supertrait used for [FloatImage]s.
pub trait FloatImageType: Float + Clone + Default + Component {}
impl<T> FloatImageType for T where T: Float + Clone + Default + Component {}

/// A fractal image (not yet a real, displayable image).
///
/// Fractal images are simply 2D buffers of [FractalResult]s. To get actual images
/// from these fractal images, see [super::Coloring]
pub type FractalImage<T> = Vector2D<FractalResult<T>>;

impl<T> FractalImage<T>
where
    T: ComplexFloatType,
{
    /// Returns a [FloatImage], consuming the [FractalImage].
    /// # Arguments
    /// * `col` - The coloring technique to use.
    pub fn to_float_image<C: Coloring<U, T>, U: FloatImageType>(
        self,
        fgi: &FractalGenInfo,
    ) -> FloatImage<U> {
        FloatImage {
            width: self.width,
            height: self.height,
            data: self
                .data
                .iter()
                .map(|fr| C::result_to_color(fr, fgi))
                .collect(),
        }
    }
}

/// An "image" (2D buffer) made of floating point numbers.
///
/// The floats represent the position of a color on a given gradient.
pub type FloatImage<T = f64> = Vector2D<T>;

impl<T> FloatImage<T>
where
    T: FloatImageType + FromF64,
{
    /// Returns an [RgbImage], consuming the [FloatImage].
    /// # Arguments
    /// * `g` - The color gradient to apply.
    pub fn to_rgb_image(self, g: &Gradient<LinSrgb<T>>) -> RgbImage {
        let mut img = RgbImage::new(self.width as u32, self.height as u32);
        for i in 0..self.height {
            for j in 0..self.width {
                // Compute the color to get from
                let color = g.get(self[(i, j)]);
                // Convert the color from the palette crate into a color usable
                // with the ImageBuffer from the image crate
                let color_u8 = [
                    (color.red * T::from(u8::MAX).unwrap()).to_u8().unwrap(),
                    (color.green * T::from(u8::MAX).unwrap()).to_u8().unwrap(),
                    (color.blue * T::from(u8::MAX).unwrap()).to_u8().unwrap(),
                ];
                img.put_pixel(j as u32, i as u32, Rgb(color_u8));
            }
        }
        img
    }
}
