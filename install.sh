#!/bin/sh

mkdir -p ~/.config/fractal-patisserie
echo "Copying gradient files"
cp gradients -r ~/.config/fractal-patisserie/
echo "Copying locations file"
cp locations.txt ~/.config/fractal-patisserie
echo "Installation successfull !"
