//! fractal-patisserie is a simple command line tool for generating fractals.
#![feature(is_sorted)]

mod gradient;
mod location;

use std::io::Read;

use fractal::location::locators::RandomLocator;
use fractal::{
    coloring::methods::Standard,
    location::{ComplexWindow, Resolution},
    mandelbrot::{Mandelbrot, MandelbrotLocator},
    FractalGenInfo,
};

use gradient::NamedGradients;
use location::NamedLocations;

fn main() {
    let mut gradient_file =
        std::fs::File::open("gradients.toml").expect("failed to open gradients.toml");
    let mut gradient_toml_str = String::new();
    gradient_file
        .read_to_string(&mut gradient_toml_str)
        .expect("failed to read gradient file");
    let gradients: NamedGradients = toml::from_str(gradient_toml_str.as_str())
        .expect("an error occured while parsing gradient file");

    let mut locations_file =
        std::fs::File::open("locations.toml").expect("failed to open locations file");
    let mut locations_file_str = String::new();
    locations_file
        .read_to_string(&mut locations_file_str)
        .expect("failed to read locations file");
    let locations: NamedLocations<f64> = toml::from_str(locations_file_str.as_str())
        .expect("an error occured while parsing locations file");

    // First, we choose the location to render in the fractal.
    // For this, we're gonna use a locator. This one is made for the Mandelbrot set:
    let mut locator = MandelbrotLocator::new();

    // Next, create a ComplexWindow, which is basically a view into the set of complex numbers.
    // Here, the window uses a random location that was returned from the locator we created
    // earlier and has a resolution of 256 x 256 pixels.
    let window: ComplexWindow = ComplexWindow::new(
        locator.get_next_random_location(),
        Resolution {
            width: 256,
            height: 256,
        },
    );

    // Next, create info to generate the wanted fractal. These usually include
    // the maximum number of iterations for the fractal sequence, and the number
    // of values obtained in the sequence to keep in memory. Those values are used
    // by some (but not all) coloring methods.

    // The Mandelbrot struct has a helper function to compute a limit for us, so let's use that:
    let fgi: fractal::FractalGenInfo =
        FractalGenInfo::new(Mandelbrot::get_ideal_limit(&window.location), None);
    // Given a ComplexWindow and a Fractal type, we can generate a FractalImage
    // using the generation info we defined earlier.
    let fractimg = window.to_fractal_image::<Mandelbrot>(&fgi);
    // The fractal image can then be converted to a float image, by specifying
    // a coloring method.
    let floatimg = fractimg.to_float_image::<Standard, f64>(&fgi);
    // The float image can then be converted to an actual RGB image (see the `image` crate for this)
    let img = floatimg.to_rgb_image(&gradients["black_and_white"].clone().into());
    // Finally, using the `image` crate, we can save the image to a file in the wanted format.
    img.save("output.png")
        .expect("Failed to save the generated image to a file");
}
