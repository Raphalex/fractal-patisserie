//! Structs related to gradients, mainly (de)serialization wrapper structures
//! and conversions with validation.

use fractal::{Gradient, LinSrgb};
use palette::{rgb::FromHexError, Component, IntoComponent};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fmt::Display, ops::Deref};

/// Serializable and deserializable version of the [LinSrgb] struct of the palette crate.
#[derive(Debug, Clone)]
struct SerializableLinSrgb<T>(LinSrgb<T>)
where
    T: Component;

impl<T> Deref for SerializableLinSrgb<T>
where
    T: Component,
{
    type Target = LinSrgb<T>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

// Implement bidirectionnal conversions for LinSrgb and its serializable variant.
impl<T> From<SerializableLinSrgb<T>> for LinSrgb<T>
where
    T: Component,
{
    fn from(l: SerializableLinSrgb<T>) -> Self {
        *l
    }
}

impl<T> From<LinSrgb<T>> for SerializableLinSrgb<T>
where
    T: Component,
{
    fn from(l: LinSrgb<T>) -> Self {
        Self(l)
    }
}

impl<'de, T> Deserialize<'de> for SerializableLinSrgb<T>
where
    T: Component,
    u8: IntoComponent<T>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        // We want to deserialize colors as string for easy user interaction
        let s = String::deserialize(deserializer)?;
        let color_u8: LinSrgb<u8> = s
            .parse()
            .map_err(|e: FromHexError| <D::Error as serde::de::Error>::custom(e.to_string()))?;

        Ok(Self(LinSrgb::<T>::from_format::<u8>(color_u8)))
    }
}

impl<T> Serialize for SerializableLinSrgb<T>
where
    T: Component,
    T: IntoComponent<u8>,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        // We want to serialize colors as string for easy user interaction
        serializer.serialize_str(format!("#{:#X}", self.into_format::<u8>()).as_str())
    }
}

/// Color associated with a weight (between 0. and 1.) to represent a control point on a gradient.
///
/// The weight is present only on weighted gradients.
#[derive(Serialize, Deserialize, Debug, Clone)]
struct GradientColor {
    /// Weight of the color
    weight: Option<f64>,
    /// Actual color
    color: SerializableLinSrgb<f64>,
}

/// Type of a gradient (the way weights are treated).
#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "lowercase")]
enum GradientType {
    /// In a linear gradient, each color is evenly spaced.
    Linear,
    /// In a weighted gradient, each color can specify a weight which places it
    /// in the gradient space (0.0 is at the start of the gradient, 1.0 at the
    /// end)
    Weighted,
}

/// Custom implementation of a gradient used to implement serde traits on.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SerializableGradient {
    #[serde(rename = "type")]
    /// Type of this gradient (see [GradientType])
    ty: GradientType,
    /// Control points
    control_points: Vec<GradientColor>,
}

impl From<ValidGradient> for Gradient<LinSrgb<f64>> {
    // Simply create a palette Gradient with the info we have
    // in the ValidGradient.
    fn from(ValidGradient(sg): ValidGradient) -> Self {
        match sg.ty {
            GradientType::Linear => {
                Gradient::new(sg.control_points.into_iter().map(|cp| *cp.color))
            }
            GradientType::Weighted => Gradient::with_domain(
                sg.control_points
                    .into_iter()
                    // Unwrap here is safe because the ValidGradient ensures
                    // that weighted gradients have ordered weights
                    .map(|cp| (cp.weight.unwrap(), *cp.color))
                    .collect(),
            ),
        }
    }
}

/// Errors that can occur during validation of a gradient. (converting from
/// [SerializableGradient] to [ValidGradient]).
#[derive(Debug)]
pub enum ValidationError {
    /// A weight is missing in a weighted gradient.
    MissingWeight,
    /// A weight is present in a linear gradient.
    UnexpectedWeight,
    /// A weight has an invalid (< 0.0 or > 1.0) value.
    InvalidWeightValue,
    /// A superior weight is placed before an inferior one in the gradient.
    InvalidWeightOrder,
}

impl Display for ValidationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::MissingWeight => write!(f, "a weight is missing in this weighted gradient, please make sure each color in this gradient has an associated weight"),
            Self::UnexpectedWeight => write!(f, "no weight should be found in linear gradients, please make sure all colors have no weight field in thie linear gradient"),
            Self::InvalidWeightValue => write!(f, "weights have to be values between 0.0 and 1.0"),
            Self::InvalidWeightOrder => write!(f, "colors must be present in increasing weight order")
        }
    }
}

/// A valid gradient, meaning, a gradient for which the fields have been checked.
///
/// This checks that the weights are correct (between 0.0 and 1.0) and that the type of
/// the gradient matches the presence of weights.
#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(try_from = "SerializableGradient")]
pub struct ValidGradient(SerializableGradient);

impl TryFrom<SerializableGradient> for ValidGradient {
    type Error = ValidationError;
    fn try_from(gr: SerializableGradient) -> Result<Self, Self::Error> {
        // This is straightforward. Each "Err" construction is an error, you can see
        // why they are returned by looking at each chain of conditions
        match gr.ty {
            GradientType::Linear => {
                if gr.control_points.iter().any(|c| c.weight.is_some()) {
                    Err(ValidationError::UnexpectedWeight)
                } else {
                    Ok(Self(gr))
                }
            }
            GradientType::Weighted => {
                if gr.control_points.iter().any(|c| c.weight.is_none()) {
                    Err(ValidationError::MissingWeight)
                } else if gr
                    .control_points
                    .iter()
                    .any(|c| !(0.0..=1.0).contains(&c.weight.unwrap()))
                {
                    Err(ValidationError::InvalidWeightValue)
                } else if !gr
                    .control_points
                    .iter()
                    .is_sorted_by(|c1, c2| c1.weight.unwrap().partial_cmp(&c2.weight.unwrap()))
                {
                    Err(ValidationError::InvalidWeightOrder)
                } else {
                    Ok(Self(gr))
                }
            }
        }
    }
}

impl Deref for ValidGradient {
    type Target = SerializableGradient;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Type alias for string-indexed gradient hashmaps.
pub type NamedGradients<'a> = HashMap<&'a str, ValidGradient>;

#[cfg(test)]
mod tests {
    use super::*;

    // We are only really interested in deserialization, so simply test this and not serialization.
    #[test]
    fn toml_deserialize_success() {
        let st = "
        [test_gradient]
        type = \"weighted\"
        control_points = [
            {weight = 0.0, color = \"#000000\"},
            {weight = 0.666, color = \"#000000\"},
            {weight = 1.0, color = \"#000000\"},
        ]
        [other_test]
        type = \"linear\"
        control_points = [
            {color = \"#abdecc\"}
        ]
        ";
        let color_s: NamedGradients = toml::from_str(st).unwrap();
        // A few quick sanity checks
        assert!(color_s.contains_key("test_gradient"));
        assert_eq!(
            format!(
                "{:#x}",
                color_s["other_test"].control_points[0]
                    .color
                    .into_format::<u8>()
            ),
            "abdecc".to_owned()
        );
        assert_eq!(
            color_s["test_gradient"].control_points[2].weight.unwrap(),
            1.0
        );
    }
}
