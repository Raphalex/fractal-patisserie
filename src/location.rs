//! Module for managing locations in fractals.

use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::ops::Deref;

use fractal::{location::Location, Complex, ComplexFloatType};

#[derive(Serialize, Deserialize)]
#[serde(remote = "Complex")]
struct ComplexDef<T> {
    re: T,
    im: T,
}

#[derive(Serialize, Deserialize)]
#[serde(remote = "Location")]
struct LocationDef<T>
where
    T: ComplexFloatType,
{
    zoom: T,
    // This field must be written last because TOML doesn't allow tables to be
    // emitted before non-table keys.
    #[serde(with = "ComplexDef")]
    pos: Complex<T>,
}

/// Wrapper type for locations in order to implement serde serialization traits.
#[derive(Serialize, Deserialize)]
#[serde(transparent)]
pub struct SerializableLocation<T>(#[serde(with = "LocationDef")] Location<T>)
where
    T: ComplexFloatType + Serialize + for<'a> Deserialize<'a>;

//// Allow dereferencing from normal Locations
impl<T> Deref for SerializableLocation<T>
where
    T: ComplexFloatType + Serialize + for<'a> Deserialize<'a>,
{
    type Target = Location<T>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> From<Location<T>> for SerializableLocation<T>
where
    T: ComplexFloatType + Serialize + for<'a> Deserialize<'a>,
{
    fn from(l: Location<T>) -> Self {
        Self(l)
    }
}

/// Type alias to use hashmaps of locations.
pub type NamedLocations<'a, T> = HashMap<&'a str, SerializableLocation<T>>;

#[cfg(test)]
mod tests {
    use super::*;
    use fractal::location::locators::DefaultLocator;
    // Test that locations can correctly be serialized and deserialized, using
    // different serialization methods.
    #[test]
    fn serde_location() {
        let loc: SerializableLocation<f64> = fractal::mandelbrot::MandelbrotLocator::new()
            .get_default_location()
            .into();
        let string_loc = toml::to_string(&loc).unwrap();
        let string_loc_deser: SerializableLocation<f64> = toml::from_str(&string_loc).unwrap();
        assert!(loc.pos == string_loc_deser.pos && loc.zoom == string_loc_deser.zoom);
        let value_loc = toml::to_vec(&loc).unwrap();
        let value_loc_deser: SerializableLocation<f64> =
            toml::from_slice(value_loc.as_slice()).unwrap();
        assert!(loc.pos == value_loc_deser.pos && loc.zoom == value_loc_deser.zoom);
    }
}
