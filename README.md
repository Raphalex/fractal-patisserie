# Fractal Patisserie

Fractal generator written in Rust

## Requirements

- cargo (https://crates.io/)

## Installation

To install the program, run the following command:

```
git clone https://gitlab.com/Raphalex/fractal-patisserie.git &&
cd fractal-patisserie &&
cargo install --path . &&
./install.sh
```

This should install the executable in `~/.cargo/bin/` and the locations and
gradients files in `~/.config/fractal-patisserie/`.

If the `./install.sh` script doesn't work, check that you have execution
rights on the file.

## Usage

For information about usage, run `fractal-patisserie --help`

## Gradients

To create new gradients, you must create a file named `<your_gradient_name>.grad`
and place it either in the `~/.config/fractal-patisserie/gradients` directory,
or in a `<path>/gradients/` directory (where `<path>` is a directory specified with
the `--config` flag)

The format of gradient files is the following: 

```
[linear]
color1 (pos1)
color2 (pos2)
...
colorn (posn)
```

where: 
- elements between `[]` are optionnal
- elements between `()` are present only when the `linear` keyword isn't used
- `posi` are floating point numbers between 0.0 and 1.0 (representing the position of the color in the gradient)
- `colori` are colors written in `rrggbb`, `#rrggbb` or `0xrrggbb` format

## Locations

The `locations.txt` file is a list of different interesting locations in the mandelbrot set.

These are used by the program to know where to place the rendering window in the fractal (improvements will need
to be made to choose completely random locations instead of generating them from a list, feel free to create a
merge request if you think you can improve the code in that way. If you have an idea for an algorithm to find
interesting spots in the fractal, but don't have the time or patience to code it, mention it in a issue)

Each entry in the location file is written like this: 

`[name] pos_re pos_im zoom_min zoom_max` 

- `name` is optionnal and is the name of the location
- `pos_re` is the real part of the position (since they are complex numbers, it's essentially the x coordinate)
- `pos_im` is the imaginary part of the position
- `zoom_min` is the minimum zoom to be used when using the `--random` flag
- `zoom_max` is the default zoom for the location, and the maximum zoom used by the `--random` flag

## Using the images as a wallpaper

If you want to generate pretty wallpapers automatically from these beautiful fractals (like me),
you should write an external script that uses `fractal-patisserie` to generate an image and set it as 
a wallpaper.

I personnaly use [nitrogen](https://wiki.archlinux.org/title/nitrogen),it's a nice utility for managing wallpapers from the command line.

## Examples

Here are some fractals generated with this program as examples:

![Roundabout](examples/roundabout.png)

![Julia](examples/julia.png)

![Another Julia set](examples/julia4.png)

![Thunder](examples/thunder.png)
